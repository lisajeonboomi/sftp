//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.results;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.constants.SFTPConstants;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class BaseResult implements Result {

	private final String statusCode;
	private final String statusMessage;
	private final OperationStatus status;
	private final Payload payload;

	public BaseResult(Payload payload, String statusCode, String message, OperationStatus status) {
		this.payload = payload;
		this.status = status;
		this.statusCode = statusCode;
		this.statusMessage = message;
	}

	public BaseResult(Payload payload) {
		this(payload, SFTPConstants.SUCCESS_CODE, SFTPConstants.SUCCESS_MESSAGE, OperationStatus.SUCCESS);
	}

	@Override
	public void addToResponse(OperationResponse response, TrackedData input) {
		response.addResult(input, this.status, this.statusCode, this.statusMessage, this.payload);
	}

	public String getStatusCode() {
		return this.statusCode;
	}

	public String getStatusMessage() {
		return this.statusMessage;
	}

	public OperationStatus getStatus() {
		return this.status;
	}

	public Payload getPayload() {
		return this.payload;
	}
}
