//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.results;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.TrackedData;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public interface Result {
    public void addToResponse(OperationResponse var1, TrackedData var2);
}

