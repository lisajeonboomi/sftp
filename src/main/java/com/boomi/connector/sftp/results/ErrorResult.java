//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.results;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.exception.NoSuchFileFoundException;
import com.boomi.connector.sftp.exception.SFTPSdkException;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class ErrorResult extends BaseResult {
	private final Throwable throwable;

	public ErrorResult(OperationStatus status, Exception e) {
		super(null, ErrorResult.inferCode(e), e.getMessage(), status != null ? status : ErrorResult.inferStatus(e));
		this.throwable = e;
	}

	public ErrorResult(Exception e) {
		this(null, e);
	}

	@Override
	public void addToResponse(OperationResponse response, TrackedData input) {
		response.addErrorResult(input, this.getStatus(), this.getStatusCode(), this.getStatusMessage(),
				this.throwable);
	}

	public static String inferCode(Exception e) {
		if (e instanceof NoSuchFileFoundException) {
			return "1";
		}
		return "-1";
	}

	public static OperationStatus inferStatus(Exception e) {
		if (e instanceof SFTPSdkException || e instanceof NoSuchFileFoundException) {
			return OperationStatus.APPLICATION_ERROR;
		}
		return OperationStatus.FAILURE;
	}
}
