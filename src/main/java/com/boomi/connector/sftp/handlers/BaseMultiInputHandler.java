//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.handlers;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.actions.RetryableVerifyFileExistsAction;
import com.boomi.connector.sftp.common.PathsHandler;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.util.StringUtil;

import java.util.logging.Level;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
abstract class BaseMultiInputHandler<T extends TrackedData> {
	final SFTPConnection connection;
	final OperationResponse operationResponse;
	final PathsHandler pathsHandler;

	BaseMultiInputHandler(SFTPConnection connection, OperationResponse operationResponse) {
		this.connection = connection;
		this.operationResponse = operationResponse;
		this.pathsHandler = this.connection.getPathsHandler();

	}

	public void processMultiInput(UpdateRequest updateRequest) {
		for (ObjectData input : updateRequest) {
			try {
				if(connection.isConnected())
				{
				this.processInput((T) input);
				}
				else {
				throw new ConnectorException("Lost Connectivity During Operation");
				}
			} catch (Exception e) {
				this.addApplicationErrorResult((T) input, e);
			}			
		}
	}

	abstract void processInput(T var1);

	private void addApplicationErrorResult(T input, Exception e) {
		input.getLogger().log(Level.WARNING, e.getMessage(), e);
		String statusCode = ErrorResult.inferCode(e);
		OperationStatus status = ErrorResult.inferStatus(e);
		this.operationResponse.addResult(input, status, statusCode, e.getMessage(), null);
	}

	String toFullPath(String childPath) {
		return this.pathsHandler.resolvePaths(connection.getHomeDirectory(), childPath);
	}

	SFTPFileMetadata extractRemoteDirAndFileName(ObjectIdData input) {
		return this.extractRemoteDirAndFileName((TrackedData) input, input.getObjectId());
	}

	SFTPFileMetadata extractRemoteDirAndFileName(TrackedData input, String filePath) {
		String enteredRemoteDir = this.connection.getEnteredRemoteDirectory(input);
         String remoteDir;
		if(StringUtil.isBlank(enteredRemoteDir) || !pathsHandler.isFullPath(enteredRemoteDir)) {
			 input.getLogger().log(Level.INFO,
						"Entered remote directory path is blank or not a absolute full path.Setting home directory of user as default working path");
			remoteDir=this.toFullPath(enteredRemoteDir);
		}
		else {
			remoteDir=enteredRemoteDir;
		}
		RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection, remoteDir,input);
		fileExistsAction.execute();
		boolean fileExists = fileExistsAction.getFileExists();

		SFTPFileMetadata fileMetadata = this.pathsHandler.splitIntoDirAndFileName(remoteDir, filePath);
		if (!fileExists)
			throw new SFTPSdkException(SFTPConstants.ERROR_REMOTE_DIRECTORY_NOT_FOUND);

		return fileMetadata;
	}
}
