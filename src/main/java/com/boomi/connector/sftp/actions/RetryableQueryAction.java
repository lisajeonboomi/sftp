//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPClient;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.common.MeteredTempOutputStream;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.util.IOUtil;
import com.boomi.util.retry.PhasedRetry;
import com.boomi.util.retry.RetryStrategy;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

import java.io.Closeable;
import java.util.logging.Level;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class RetryableQueryAction implements Closeable {
	public boolean isReconnectFailed() {
		return reconnectFailed;
	}

	public void setReconnectFailed(boolean reconnectFailed) {
		this.reconnectFailed = reconnectFailed;
	}

	private static final Object NULL_STATUS = null;
	private final SFTPConnection connection;
	private final String remoteDir;
	private final TrackedData input;
	private final RetryStrategyFactory retryFactory;
	private final SFTPClient client;
	private final String filePath;
	private final MeteredTempOutputStream outputStream = new MeteredTempOutputStream();
	private boolean reconnectFailed;

	public MeteredTempOutputStream getOutputStream() {
		return outputStream;
	}

	public RetryableQueryAction(SFTPConnection connection, String remoteDir, TrackedData input,
			RetryStrategyFactory retryFactory, SFTPClient client, String filePath) {
		this.connection = connection;
		this.remoteDir = remoteDir;
		this.input = input;
		this.retryFactory = retryFactory;
		this.client = client;
		this.filePath = filePath;
	}

	public TrackedData get_input() {
		return input;
	}

	public void execute() {
		RetryStrategy retry = this.retryFactory.createRetryStrategy();
		int numAttempts = 0;
		do {
			try {
				this.doExecute();
				return;
			} catch (SFTPSdkException e) {

				if (((SftpException) e.getCause()).id == ChannelSftp.SSH_FX_CONNECTION_LOST
						|| ((SftpException) e.getCause()).id == ChannelSftp.SSH_FX_NO_CONNECTION
						|| ((SftpException) e.getCause()).id == ChannelSftp.SSH_FX_FAILURE) {
					if (!retry.shouldRetry(++numAttempts, NULL_STATUS)) {
						throw e;
					}
					this.input.getLogger().log(Level.WARNING,
							"Failure Occured.Attempting to Retry the action. Retry attempt no " + numAttempts);
					this.reconnect();
				} else {
					throw e;
				}
			} catch (ConnectorException e) {
				if (!retry.shouldRetry(++numAttempts, NULL_STATUS)) {
					throw e;
				}
				this.input.getLogger().log(Level.WARNING,
						"Failure Occured.Attempting to Retry the action. Retry attempt no " + numAttempts);
				this.reconnect();
			}

		} while (true);
	}

	public void doExecute() {
		this.client.openConnection();
		this.getConnection().getFile(this.filePath, this.outputStream, this.client);
	}

	private void reconnect() {
		PhasedRetry reconnectRetry = new PhasedRetry();
		int numAttempts = 0;
		do {
			try {
				this.input.getLogger().log(Level.WARNING, "Attempting to re-establish connection with remote system,"
						+ "Phased Reconnect attempt no: {0} ", (numAttempts + 1));
				this.client.openConnection();
				this.input.getLogger().log(Level.WARNING,
						"Established connectivity with the remote system.Resuming operation");
				return;
			} catch (ConnectorException e) {
				if (!reconnectRetry.shouldRetry(++numAttempts, NULL_STATUS)) {
					reconnectFailed = true;
					throw e;
				}
				this.input.getLogger().log(Level.WARNING,
						"Attempted re-establishing connection, backing off. Cause: " + e.getMessage(), e);
				long timeBeforeBackoff = System.currentTimeMillis();
				reconnectRetry.backoff(numAttempts);
				long timeAfterBackoff = System.currentTimeMillis();
				this.input.getLogger().log(Level.WARNING,
						"Waited for " + (timeAfterBackoff - timeBeforeBackoff) + " milliseconds");

			}

		} while (true);
	}

	public SFTPConnection getConnection() {
		return this.connection;
	}

	String getRemoteDir() {
		return this.remoteDir;
	}

	@Override
	public void close() {
		IOUtil.closeQuietly((Closeable[]) new Closeable[] { this.outputStream });
	}
}
