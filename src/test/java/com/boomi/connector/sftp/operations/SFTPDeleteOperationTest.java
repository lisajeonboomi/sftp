package com.boomi.connector.sftp.operations;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.sftp.SFTPClient;
import com.boomi.connector.sftp.SFTPConnector;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.connector.testutil.SimpleOperationContext;
import com.boomi.connector.testutil.SimpleTrackedData;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest(SFTPClient.class)
public class SFTPDeleteOperationTest { 

	@Test
	public void testExecuteDeleteDeleteRequestOperationResponse() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		SftpException ex = new SftpException(2, "File Exists");
		// when(channel.stat(any(String.class))).thenThrow(ex);
		// ChannelSftp.LsEntry ab=
		Vector vect = new Vector();
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);

		
		when(channel.lstat(any(String.class))).thenReturn(attrs);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS, "OVERWRITE");
		opProperty.put(SFTPConstants.PROPERTY_STAGING_DIRECTORY,"/abc");
		//opProperty.put(SFTPConstants.PROPERTY_TEMP_EXTENSION,"abc");
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);

		SimpleOperationContext opContext = new SimpleOperationContext(null, null, OperationType.CREATE, null, null,
				null, null);
		SFTPConnector connector = new SFTPConnector();

		ConnectorTester tester = new ConnectorTester(connector);
		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");
		tester.setOperationContext(OperationType.DELETE, connProperty, opProperty, "File", null);
		// tester.setBrowseContext(context);
		tester.setBrowseContext(OperationType.DELETE, connProperty, opProperty); 
		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);
		List<String> objectIds= new ArrayList<>();
		objectIds.add("abc");
		objectIds.add("xyz");
		tester.executeDeleteOperation(objectIds);
	}

	@Test
	public void testExecuteDeleteDeleteRequestOperationResponseDeleteFailed() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		SftpException ex = new SftpException(2, "File Exists");
		// when(channel.stat(any(String.class))).thenThrow(ex);
		// ChannelSftp.LsEntry ab=
		Vector vect = new Vector();
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
      doThrow(ex).when(channel).rm(any(String.class));
		//when(channel.rm(any(String.class))).;
		when(channel.lstat(any(String.class))).thenReturn(attrs);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS, "OVERWRITE");
		opProperty.put(SFTPConstants.PROPERTY_STAGING_DIRECTORY,"/abc");
		//opProperty.put(SFTPConstants.PROPERTY_TEMP_EXTENSION,"abc");
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);

		SimpleOperationContext opContext = new SimpleOperationContext(null, null, OperationType.CREATE, null, null,
				null, null);
		SFTPConnector connector = new SFTPConnector();

		ConnectorTester tester = new ConnectorTester(connector);
		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");
		tester.setOperationContext(OperationType.DELETE, connProperty, opProperty, "File", null);
		// tester.setBrowseContext(context);
		tester.setBrowseContext(OperationType.DELETE, connProperty, opProperty); 
		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);
		List<String> objectIds= new ArrayList<>();
		objectIds.add("abc");
		objectIds.add("xyz");
		tester.executeDeleteOperation(objectIds);
	}
	

	@Test
	public void testExecuteDeleteDeleteRequestOperationResponseDeleteFailedException() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		SftpException ex = new SftpException(2, "File Exists");
		// when(channel.stat(any(String.class))).thenThrow(ex);
		// ChannelSftp.LsEntry ab=
		Vector vect = new Vector();
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
      doThrow(new RuntimeException()).when(channel).rm(any(String.class));
		//when(channel.rm(any(String.class))).;
		when(channel.lstat(any(String.class))).thenReturn(attrs);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS, "OVERWRITE");
		opProperty.put(SFTPConstants.PROPERTY_STAGING_DIRECTORY,"/abc");
		//opProperty.put(SFTPConstants.PROPERTY_TEMP_EXTENSION,"abc");
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);

		SimpleOperationContext opContext = new SimpleOperationContext(null, null, OperationType.CREATE, null, null,
				null, null);
		SFTPConnector connector = new SFTPConnector();

		ConnectorTester tester = new ConnectorTester(connector);
		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");
		tester.setOperationContext(OperationType.DELETE, connProperty, opProperty, "File", null);
		// tester.setBrowseContext(context);
		tester.setBrowseContext(OperationType.DELETE, connProperty, opProperty); 
		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);
		List<String> objectIds= new ArrayList<>();
		objectIds.add("abc");
		objectIds.add("xyz");
		tester.executeDeleteOperation(objectIds);
	}
	
	@Test
	public void testExecuteDeleteDeleteRequestOperationResponseDeleteFailedOpenConnection() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenThrow(new RuntimeException());
		SftpException ex = new SftpException(2, "File Exists");
		// when(channel.stat(any(String.class))).thenThrow(ex);
		// ChannelSftp.LsEntry ab=
		Vector vect = new Vector();
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
      doThrow(new RuntimeException()).when(channel).rm(any(String.class));
		//when(channel.rm(any(String.class))).;
		when(channel.lstat(any(String.class))).thenReturn(attrs);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS, "OVERWRITE");
		opProperty.put(SFTPConstants.PROPERTY_STAGING_DIRECTORY,"/abc");
		//opProperty.put(SFTPConstants.PROPERTY_TEMP_EXTENSION,"abc");
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);

		SimpleOperationContext opContext = new SimpleOperationContext(null, null, OperationType.CREATE, null, null,
				null, null);
		SFTPConnector connector = new SFTPConnector();

		ConnectorTester tester = new ConnectorTester(connector);
		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");
		tester.setOperationContext(OperationType.DELETE, connProperty, opProperty, "File", null);
		// tester.setBrowseContext(context);
		tester.setBrowseContext(OperationType.DELETE, connProperty, opProperty); 
		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);
		List<String> objectIds= new ArrayList<>();
		objectIds.add("abc");
		objectIds.add("xyz");
		tester.executeDeleteOperation(objectIds);
	}
}
